use std::fs::read_to_string;
use std::vec; 
use std::slice::Iter; 
use std::time::Duration;
use std::thread::sleep; 
use std::rc::Rc;

use anyhow::{Result, bail, Ok};
use rand::{thread_rng, Rng};

enum Direction {
    UP,
    RIGHT,
    DOWN,
    LEFT
}

#[derive(PartialEq, Debug)]
enum CellContent {
    START,
    PATH,
    WALL,
    EXIT,
    VISITED
}

#[derive(PartialEq, Debug)]
struct Position {
    x: usize,
    y: usize
}

#[derive(PartialEq, Debug)]
struct Cell {
    position: Position,
    content: CellContent
}

type TMaze = Vec<Vec<char>>;

struct Maze {
    maze: TMaze
}

const POSSIBLE_DIRECTIONS: [Direction; 4] = [Direction::UP, Direction::RIGHT, Direction::DOWN, Direction::LEFT];

fn index_of<T>(iter: &mut Iter<'_, T>, target: &T) -> usize
where
    T: PartialEq 
{
    iter.position(|item| item == target).unwrap()
}

impl Maze {
    fn new(maze: TMaze) -> Self { Self { maze } }

    fn print_maze(&self) {
        for row_index in 0..self.maze[usize::default()].len() {
            self.maze.iter().for_each(|col| print!("{}", col[row_index]));
            println!();
        }
    }
    
    fn find_start_position(&self) -> Position {    
        for (x, col) in self.maze.iter().enumerate() {
            for (y, row) in col.iter().enumerate() {
                if *row == 'S' {
                    return Position { x, y }
                }
            }   
        }

        Position { x: usize::default(), y: usize::default() }
    }

    fn update_maze_cell_content(&mut self, moved_pos: &Position, new_content: char) {
        self.maze[moved_pos.x][moved_pos.y] = new_content;
    }
    
    fn handle_valid_coordinate(&self, coordinate: i32) -> Result<usize> {    
        if coordinate.is_positive() || coordinate == 0 {
            let positive_coordinate: usize = coordinate.try_into()?;
    
            if positive_coordinate < self.maze.len() {
                return Ok(positive_coordinate)
            } else {
                bail!("Coordinate {} is more or equal to the adapted maze size!", positive_coordinate);
            }
        } else {
            bail!("Coordinate {} is negative!", coordinate);
        }
    }
    
    fn handle_direction_cell(&self, curr_pos: &Position, dir: Direction) -> Result<Cell> {
        let curr_pos_int: (i32, i32) = (curr_pos.x.try_into()?, curr_pos.y.try_into()?);
    
        let vector: (i32, i32) = match dir {
            Direction::UP => (0, -1),
            Direction::RIGHT => (1, 0),
            Direction::DOWN => (0, 1),
            Direction::LEFT => (-1, 0)
        };
    
        let dir_pos = Position {
            x: self.handle_valid_coordinate(curr_pos_int.0 + vector.0)?,
            y: self.handle_valid_coordinate(curr_pos_int.1 + vector.1)?
        };
    
        let cell_content = match self.maze[dir_pos.x][dir_pos.y] {
            'S' => CellContent::START,
            '.' => CellContent::PATH,
            '#' => CellContent::WALL,
            'E' => CellContent::EXIT,
            '*' => CellContent::VISITED,
            _ => bail!(
                "Cell content on coordinates [{}, {}] has character {}, which is invalid for the maze!", 
                dir_pos.x, 
                dir_pos.y, 
                self.maze[dir_pos.x][dir_pos.y]
            )
        };
        
        Ok(Cell { position: dir_pos, content: cell_content })
    }
    
    fn handle_movement(&self, curr_pos: &Position) -> Result<(Rc<Cell>, bool)> {
        let mut visited_possible_dir_cells: Vec<Rc<Cell>> = vec![];
        let mut is_blind = false;

        for dir in POSSIBLE_DIRECTIONS {
            let dir_cell = self.handle_direction_cell(curr_pos, dir)?;
            if dir_cell.content == CellContent::EXIT {
                return Ok((Rc::new(Cell { position: dir_cell.position, content: dir_cell.content }), false))
            } else if dir_cell.content != CellContent::WALL {
                visited_possible_dir_cells.push(Rc::new(dir_cell));
            }
        }

        let mut selected_path_index = usize::default();

        if visited_possible_dir_cells.len() > 1 {
            let mut possible_paths: Vec<&Rc<Cell>> = vec![];
            let mut idx = usize::default();

            visited_possible_dir_cells.iter().for_each(|cell| {
                if cell.content == CellContent::PATH {
                    possible_paths.push(&cell);
                }
            });

            if possible_paths.len() > 1 {
                idx = thread_rng().gen_range(0..possible_paths.len());
            }
            
            selected_path_index = index_of(&mut visited_possible_dir_cells.iter(), possible_paths[idx]);
        } else {
            if visited_possible_dir_cells[usize::default()].content == CellContent::VISITED {
                is_blind = true;
            }
        }

        Ok((
            Rc::clone(&visited_possible_dir_cells[selected_path_index]),
            is_blind
        ))
    }

    fn handle_blind_path_escape(&self, pos: &Position) -> Result<bool> {
        for dir in POSSIBLE_DIRECTIONS {
            let dir_cell = self.handle_direction_cell(&pos, dir)?;
            
            if dir_cell.content != CellContent::VISITED && dir_cell.content != CellContent::WALL {
                return Ok(true)
            }
        }

        Ok(false)
    }
    
    fn solve_maze(&mut self) -> Result<()> {
        let start_pos = self.find_start_position();
        let mut solved = false;
        let mut stack: Vec<Rc<Cell>> = vec![];

        stack.push(Rc::new(Cell { position: start_pos, content: CellContent::START }));
    
        while !solved {
            let moved_cell = self.handle_movement(&stack[stack.len() - 1].position)?;

            if moved_cell.1 {
                let mut cell_until_escape = &stack[stack.len() - 1];
                let mut escaped = false;

                while !escaped {
                    escaped = self.handle_blind_path_escape(&cell_until_escape.position)?;
                    cell_until_escape = &stack[index_of(&mut stack.iter(), &cell_until_escape) - 1];
                }

                let escaped_pos_index = index_of(&mut stack.iter(), &cell_until_escape);

                for i in ((escaped_pos_index + 2)..stack.len()).rev() {
                    stack.remove(i);
                }

                continue;
            }

            if moved_cell.0.content == CellContent::EXIT {
                // 'W' => win
                self.update_maze_cell_content(&moved_cell.0.position, 'W');
                solved = true;
            } else if moved_cell.0.content != CellContent::START {
                // '*' => visited path
                self.update_maze_cell_content(&moved_cell.0.position, '*');
                self.print_maze();
            }

            if !stack.contains(&moved_cell.0) {
                stack.push(Rc::clone(&moved_cell.0));
            }

            sleep(Duration::from_millis(100));
            // cleaning printed output
            print!("\x1B[2J");
            print!("\x1B[1;1H");
        }

        self.print_maze();

        Ok(())
    }
}

fn read_maze(file_name: &str) -> Result<TMaze> {
    let content = read_to_string(&file_name)?;
    let mut maze: TMaze = vec![];
    let mut horizontal_index = usize::default();
    let mut line_length = usize::default();
    let mut filled_correctly = false;

    while !filled_correctly {
        maze.push(vec![]);

        content.lines().into_iter().for_each(|line| {
            maze[horizontal_index].push(line.as_bytes()[horizontal_index] as char);

            if line_length == usize::default() {
                line_length = line.as_bytes().len();
            }
        });

        if horizontal_index >= line_length - 1 {
            filled_correctly = true;
        } else {
            horizontal_index += 1;
        }
    }

    Ok(maze)
}

fn main() -> Result<()> {
    const FILE_NAME: &str = "maze.txt";

    let mut maze = Maze::new(read_maze(FILE_NAME)?);
    
    maze.solve_maze()?;

    Ok(())
}
